#ifndef GAMESCREEN_H_
#define GAMESCREEN_H_

#include <chrono>
#include <map>

#include <SDL.h>

#include <core/point.h>
#include <game/screen.h>
#include <game/collection.h>
#include <game/camera.h>

class Image;
enum class Landscape;

namespace castle
{
    namespace world
    {
        class Creature;
        class SimulationManager;
    }
    namespace ui
    {
        class ScreenManager;
    }
}

namespace castle
{
    namespace ui
    {
        class GameScreen : public Screen
        {
        public:
            explicit GameScreen(ScreenManager &screenManager);
            GameScreen(GameScreen const&) = delete;
            GameScreen& operator=(GameScreen const&) = delete;
            virtual ~GameScreen();

            void SetSimulation(world::SimulationManager &simsim);
            void Render(render::Renderer &renderer);
            bool HandleEvent(const SDL_Event &event);
            world::Camera& GetActiveCamera();

        protected:
            void RenderTile(render::Renderer &renderer, const world::MapCell &cell);
            void RenderCreature(render::Renderer &renderer, const world::Creature &creature);
            bool HandleKeyPress(const SDL_KeyboardEvent &event);
            bool HandleMouseButton(const SDL_MouseButtonEvent &event);
            void UpdateCamera(const render::Renderer &renderer);
            void ToggleCameraMode();
            bool IsTileSelected(const core::Point &cursor, const world::MapCell &cell) const;
            world::MapCell FindSelectedTile(const render::Renderer &renderer);
            gfx::Collection const& GetTileSet(const Landscape &landscape) const;

        private:
            ScreenManager &mScreenManager;
            gfx::Collection archer;
            gfx::Collection landset;
            gfx::Collection seaset;
            gfx::Collection rockset;
            gfx::Collection cliffs;
            std::chrono::steady_clock::time_point mLastCameraUpdate;
            std::map<SDL_Keycode, bool> mKeyState;
            core::Point mCursor;
            bool mCursorInvalid;
            world::Camera mCamera;
            world::SimulationManager *mSimManager;
        };
    }
}

#endif  // GAMESCREEN_H_
