#ifndef CAMERAMODE_H_
#define CAMERAMODE_H_

namespace castle
{
    namespace world
    {
        enum class CameraMode : int {
            Staggered,
            Diamond,
            Ortho
        };
    }
}

#endif // CAMERAMODE_H_
