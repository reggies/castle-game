#ifndef SIMULATIONTICK_H_
#define SIMULATIONTICK_H_

namespace castle
{
    namespace world
    {
        using tick_t = unsigned long long;
    }
}

#endif // SIMULATIONTICK_H_
