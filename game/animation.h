#ifndef ANIMATION_H_
#define ANIMATION_H_

enum class Animation : int
{
    Walk,
    Run,
    Idle1,
    Idle2,
    Idle3,
    Dig,
    Climb,
    RemoveLadder,
    DeathArrow,
    DeathLunge,
    DeathPunch,
    FallLadder,
    AttackMelee,
    Triumph                                              // swordman, mace
};

#endif
