#include "graphicsmanager.h"

namespace castle
{
    namespace gfx
    {
        GraphicsManager::~GraphicsManager() = default;
        GraphicsManager::GraphicsManager()
        {
        }
    }
}
