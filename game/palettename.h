#ifndef PALETTENAME_H_
#define PALETTENAME_H_

#include <cstddef>

namespace castle
{
    namespace gfx
    {
        enum class PaletteName
        {
            Blue         = 1,
            Red          = 2,
            Orange       = 3,
            Yellow       = 4,
            Purple       = 5,
            Black        = 6,
            Cyan         = 7,
            Green        = 8,
        };
    }
}

#endif // PALETTENAME_H_
