#ifndef MODES_H_
#define MODES_H_

namespace
{
    // Common string literals for option names
    const char *kFile = "file";
    const char *kIndex = "index,i";
    const char *kPalette = "palette,p";
    const char *kTilePart = "tile-part";
    const char *kBinary = "binary";
    const char *kCount = "count";
    const char *kType = "type";
    const char *kFormat = "format,f";
    const char *kTransparentColor = "transparent-color";
}

#endif // MODES_H_
